import { useDispatch, useSelector } from "react-redux";
import 'antd/dist/antd.css';
import { UserOutlined } from '@ant-design/icons';
import { Dropdown, Menu, message, Space } from 'antd';
import React from 'react';
import { applicationsActions } from "../../store/actions/applicationsActions";

const Table = () => {
    const dispatch = useDispatch();
    let items = useSelector((state) => state.items);

    const handleButtonClick = (e, name) => {
        const changeItems = items.map((item) => {
          if (item.name === name) {
              item.active = true;
          } else {
              item.active = false;
          }

          return item;
        });

        dispatch({
            type: applicationsActions.GET_ONE_ASYNC,
            changeItems
          })
      };
      
      const handleMenuClick = (e) => {
        message.info('Click on menu item.');
        console.log('click', e);
    };

    return (
        <Space wrap>
            {
                items.map((item) => (
                    <Dropdown.Button key={item.id} onClick={(e) => handleButtonClick(e, item.name)} overlay={<Menu
                        onClick={handleMenuClick}
                        items={
                            item.items.map((content) => (
                                {
                                    label: content.name,
                                    key: content.id,
                                    icon: <UserOutlined />, 
                                }
                            ))}
                      />}>
                        {item.name}
                    </Dropdown.Button>
                ))
            }
        </Space>
    )
}

export default Table;