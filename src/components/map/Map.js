import { useSelector } from 'react-redux';
import { MapContainer, TileLayer, Popup, Polyline, CircleMarker } from 'react-leaflet';

const Map = () => {
    const items = useSelector(state => state.items);

    return (
        <MapContainer center={[55.15983438693836, 61.398447118644015]} zoom={13} scrollWheelZoom={true}>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {
            items.map((item) => (
                    item.items.map((content) => (
                        <CircleMarker key={content.id} center={[content.coordinate1, content.coordinate2]} pathOptions={{ fillColor: item.color, color: item.color }} radius={20}>
                            <Popup>
                            {content.description} <br /> {content.name}
                            </Popup>
                        </CircleMarker>
                    ))
                ))
            }

            {
                items.map((item) => (
                    item.active === true ? (
                        <Polyline key={item.id} pathOptions={{ color: item.color }} positions={item.polyline} />
                    ) : null
                ))
            }
      </MapContainer>
    )
}

export default Map;