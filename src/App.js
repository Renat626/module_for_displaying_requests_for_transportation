import AppCss from "./App.css";
import Map from "./components/map/Map";
import Table from "./components/table/Table";

function App() {
  return (
    <div className="App">
      <Table />
      <Map />
    </div>
  );
}

export default App;
