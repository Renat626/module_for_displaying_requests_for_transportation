import {put, takeEvery} from "redux-saga/effects";
import { getOne } from "../store/actionsCreator/applicationsActionsCreator";
import { applicationsActions } from "../store/actions/applicationsActions";

const delay = (ms) => new Promise(res => setTimeout(res, ms));

function* chooseApplication(items) {
    yield delay(300);
    yield put(getOne(items.changeItems))
}

export function* watcher() {
    yield takeEvery(applicationsActions.GET_ONE_ASYNC, chooseApplication);
}