import { applicationsActions } from "../actions/applicationsActions";
import { applicationsState } from "../states/applicationsState"; 

export const getOne = (payload) => (
    {
        type: applicationsActions.GET_ONE,
        payload
    }
)

export const getOneAsync = (payload) => (
    {
        type: applicationsActions.GET_ONE_ASYNC,
        payload
    }
)