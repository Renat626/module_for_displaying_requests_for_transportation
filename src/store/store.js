import { createStore, applyMiddleware } from "redux";
import { applicationsReducer } from "./reducers/applicationsReducer";
import createSagaMiddleware from 'redux-saga'
import { watcher, rootSaga } from "../saga/chooseApplication";

const sagaMiddleware = createSagaMiddleware()

const reducer = applicationsReducer;

export const store = createStore(reducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watcher);