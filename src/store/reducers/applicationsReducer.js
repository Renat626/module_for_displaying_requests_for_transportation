import { applicationsState } from "../states/applicationsState";
import { applicationsActions } from "../actions/applicationsActions";

export const applicationsReducer = (state = applicationsState, action) => {
    switch (action.type) {
        case applicationsActions.GET_ONE:
            return {...state, items: action.payload}
        default:
            return state;
    }
}